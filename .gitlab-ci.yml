---
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
  - test
  - deploy

variables:
  helm_version: 3.12.2

.charts-matrix: &charts-matrix
  parallel:
    matrix:
      - CHART: [aggregator, dariah-de-documentation, repdav, tg-fcs-endpoint, ebookportal, tg-beacon-service, tg-iiif-presentation]

default:
  # https://gitlab.com/gitlab-org/cluster-integration/helm-install-image
  image: registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image:helm-3.12.2-kube-1.27.4-alpine-3.18.2

helm lint:
  stage: test
  <<: *charts-matrix
  script:
    - helm lint ./${CHART} --strict --values ./${CHART}/values.yaml
  rules:
    - if: $CI_MERGE_REQUEST_IID
      changes:
        - $CHART/**/*

helm dry-run:
  # https://gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci
  stage: test
  <<: *charts-matrix
  services:
    - name: registry.gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/releases/v1.30.0-k3s1
      alias: k3s
      command: ["server", "--disable-agent", "--tls-san=k3s"]
  before_script:
    - apk add curl
    - curl -f k3s:8081 > k3s.yaml
    - export KUBECONFIG=$(pwd)/k3s.yaml
    - kubectl version
    - kubectl cluster-info
  script:
    - helm install --dry-run --debug ${CHART} ./${CHART}
  rules:
    - if: $CI_MERGE_REQUEST_IID
      changes:
        - $CHART/**/*

helm check version:
  stage: test
  <<: *charts-matrix
  before_script:
    - apk add yq
  script:
    - version=$(yq eval '.version' ${CHART}/Chart.yaml)
    # TODO Use HARBOR_URL here if adapted to harbor.gwdg.de!
    - helm registry login --username="$HARBOR_USER" --password="$HARBOR_PASS" harbor.gwdg.de
    - helm show chart oci://harbor.gwdg.de/sub-fe/${CHART} --version=$version || exit_code=$?
    - if [ $exit_code -ne 0 ]; then echo "version not existing."; else echo "version existing"; exit 1; fi;
  rules:
    - if: $CI_MERGE_REQUEST_IID
      changes:
        - $CHART/**/*

harbor-upload:
  # TODO Check and adapt HARBOR_URL and HARBOR_CHART_REPO in DARIAH CI/CD variables!
  stage: deploy
  <<: *charts-matrix
  image:
    name: alpine/helm:${helm_version}
    entrypoint: [""] # override entrypoint to use sh instead of starting helm directly
  before_script:
    # TODO Use HARBOR_URL here if adapted to harbor.gwdg.de!
    - helm registry login --username="$HARBOR_USER" --password="$HARBOR_PASS" harbor.gwdg.de
  script:
    - helm package ${CHART}
    # TODO Use HARBOR_CHART_REPO here if adapted to OCI!
    - helm push ${CHART}*.tgz oci://harbor.gwdg.de/sub-fe
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - $CHART/**/*
