# Helm Charts

## Chart Readme File Generation

You could generate a decent `README.md` using [helm-docs](https://github.com/norwoodj/helm-docs). For your convenience, a shell-script using the docker image of the tool is provided with this repository.

```shell
./helm-docs.sh
```

See [repdav/README.md](repdav/README.md) for an example of the output.

## Testing Helm charts locally

check your active context

```shell
kubectl config get-contexts
```

change if not on dev cluster

```shell
kubectl config use-context sub-dev
```

and install your helm chart (into namespace textgrid-test, with some env vars set)

```shell
helm install --set env.TEXTGRID_HOST=https://dev.textgridlab.org --set env.TGREP_PORTAL_HOST=https://dev.textgridrep.org tg-fcs tg-fcs-endpoint -n textgrid-test
```

follow the instructions for local port forwarding, and if everything is fine uninstall

```shell
helm uninstall tg-fcs -n textgrid-test
```

